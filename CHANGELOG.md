# changes by release
## 0.3.5
### fixes
* zpool_usage: fixed percentage used and available space to match what is displayed in the GUI.

## 0.3.4
### features
* new mode:
  * system_info: return FreeNAS version and uptime status.
    example:  "OK - FreeNAS-11.3-U5, 2:50PM  up 12 days, 22:13, 0 users"

### other
* alerts: modified to append the datetime of the alert info to the returned message (if datetime field present).
  example:  "OK - Scrub of pool 'ZMPR502' finished. (2021-01-15 12:12:31 GMT)"
* zpool_usage: modified to return the status of the specified ZPOOL (e.g. HEALTHY, etc) as well as the percentage of space used.
  example:  "OK - ZMPR502: status HEALTHY, 71% used."
* Updated version tag.

## 0.3.3
### fixes
* alerts: fixes service output when all alerts are dismissed 

## 0.3.2
### fixes
* alerts, updates: now uses api v2.0

## 0.3.1
### fixes
* datasets_usage: increase the limit query param to 50

### other
* include and exclude option now uses regex instead of array

## 0.3.0
### features
* new modes:
  * zpool_iostat_read_iops
  * zpool_iostat_write_iops
  * zpool_iostat_read_bw
  * zpool_iostat_write_bw

### other
* updated tested versions

## 0.2.0
### features
* new modes:
 * zpool_usage
 * dataset_quota
 * arc_cache_miss_ratio
 * arc_cache_meta_usage
 * arc_cache_mru_ghost
 * arc_cache_mfu_ghost
 * io_requests_pending
 * disk_busy

### other
* no *https://* prefix necessary anymore for address
* renamed mode *datasets* to *datasets_usage*
* refactored some helper methods


## 0.1.1
### fixes
* *alert_check*: convert empty json output to OK status

### other
* catch all http responses instead of a few

## 0.1
* Initial release
