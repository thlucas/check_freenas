# check_freenas

[FreeNAS](http://www.freenas.org/) Naemon/Icinga/Nagios plugin.

[[_TOC_]]

## Requirements
* Ruby >2.3

* Some checks are executed via SSH (see below). For this the [net-ssh](https://rubygems.org/gems/net-ssh) gem is necessary.
```shell
gem install net-ssh
```

## Options
* -s: FreeNAS host, *freenas.example.com*, mandatory
* -u: username, mandatory
* -k: if you've a self signed cert
* -z: zpool name
* -d: dataset name
* -i: only use this dataset (regex)
* -x: exclude dataset (regex)

## Modes
### API
Following checks are performed via the FreeNAS API (v1.0, v2.0). Only https and user *root* is possible.

#### Alerts
Check outstanding [alerts](https://www.ixsystems.com/documentation/freenas/11.2-U7/alert.html). (API v2.0, FreeNAS: >11.2)
```shell
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m alerts
Warning - smartd is not running
OK - Scrub of pool 'TANK01' finished. (2021-01-15 12:12:31 GMT)
```

#### System_Info
Returns FreeNAS version and uptime stats. (API v2.0, FreeNAS: >11.2)
```shell
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m system_info
OK - FreeNAS-11.3-U5, 2:50PM  up 12 days, 22:13, 0 users
```

#### Updates
Notifies of available updates. (API v2.0, FreeNAS: >11.2)
```shell
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m updates
Warning - There are pending updates. Go to System > Update to apply pending updates
```

#### Datasets usage
Check used space in MB of all datasets with perfdata. (API v1.0)
Use include/exclude option to limit output.
```shell
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m datasets_usage -z tank -i '^(data|home)' -w 1176000 -c 1185555
Warning - Dataset data used 1176133MB | home=910786MB;1176000;1185555 data=1176133MB;1176000;1185555
```

#### Zpool usage
Checks Zpool usage in percentage with perfdata. (API v1.0)
```shell
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m zpool_usage -w 80 -c 90 -z tank
OK - tank: status HEALTHY, 74% used (2.61 TB available). | tank=74%;80;90
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m zpool_usage -w 60 -c 90 -z tank
Warning - tank: status HEALTHY, 74% used (2.61 TB available). | tank=74%;60;90
./check_freenas.rb -s freenas.example.com -u root -p <PASSWORD> [-k] -m zpool_usage -w 60 -c 70 -z tank
Critical - tank: status HEALTHY, 74% used (2.61 TB available). | tank=74%;70;90
```

### SSH
Following checks are performed through SSH via [net-ssh](https://rubygems.org/gems/net-ssh) to the FreeNAS host.
SSH connection is initiated from the host who executes the plugin.


#### Dataset quota
Check used space against refquota value. Output will be in percentage (float) with perfdata.
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m dataset_quota_usage -w 70 -c 80 -d tank/data
Critical - tank/data: usage 85.98% | tank/data=85.98%;30;40
```

#### ARC Cache
Checks various kstat values. Output will be in percentage (float) with perfdata.

##### Miss ratio
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m arc_cache_miss_ratio -w 60 -c 70
Critical - ARC cache miss ratio: 70.72% | arc_cache_miss_ratio=70.72%;60;70
```
##### Metadata usage
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m arc_cache_meta_usage -w 60 -c 70
OK - ARC cache metadata usage: 19.61% | arc_cache_meta_usage=19.61%;60;70
```

##### MRU ghost hits
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m arc_cache_mru_ghost -w 10 -c 15
OK - ARC cache mru ghost hits: 0.02% | arc_cache_mru_ghost=0.02%;10;15
```

##### MFU ghost hits
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m arc_cache_mfu_ghost -w 10 -c 15
OK - ARC cache mfu ghost hits: 2.46% | arc_cache_mfu_ghost=2.46%;10;15
```

#### IO requests pending
Checks pending io in queue via gstat from all physical disks that are at least 0.1% busy.
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m io_requests_pending -w 5 -c 10
OK - ada0: 0, da0: 0, da18: 0, da22: 0, da24: 0, da25: 0, da27: 1, da28: 0, da30: 0, da32: 0, da33: 0 | ada0=0;5;10 da0=0;5;10 da18=0;5;10 da22=0;5;10 da24=0;5;10 da25=0;5;10 da27=1;5;10 da28=0;5;10 da30=0;5;10 da32=0;5;10 da33=0;5;10
```

#### Disk busy
Checks disk busy value via `gstat` from all physical disks that are at least 0.1% busy.
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m disk_busy -w 40 -c 50
OK - ada0: 0.3%, da0: 0.3%, da21: 1.6%, da22: 0.6%, da25: 1.7%, da26: 3.1%, da28: 1.3%, da30: 1.3%, da31: 0.5%, da33: 1.0% | ada0=0.3%;40;50 da0=0.3%;40;50 da21=1.6%;40;50 da22=0.6%;40;50 da25=1.7%;40;50 da26=3.1%;40;50 da28=1.3%;40;50 da30=1.3%;40;50 da31=0.5%;40;50 da33=1.0%;40;50
```

#### Zpool IOPS
Checks read/write IOPS via `zpool iostat` command (value rounded).
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m zpool_iostat_read_iops -z <ZPOOL> -w 400 -c 500
Critical - Read IOPS: 514.0 | read_iops=514.0;30;50
```

#### Zpool Bandwith
Checks read/write bandwith in MB via `zpool iostat` command (value rounded).
```shell
./check_freenas.rb -s freenas.example.com -u <USER> -p <PASSWORD> -m zpool_iostat_write_bw -z <ZPOOL> -w 30 -c 50
OK - Write bandwith: 10.6MB | write_bw=10.6MB;30;50
```
